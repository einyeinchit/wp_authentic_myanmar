<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_authentic_myanmar');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '01)>z6o)1B<r[<uvCG~Y3=HyD}1(uFB!-t6a~gZQkfL+(XrGLKI%j:J{4@N3J/XH');
define('SECURE_AUTH_KEY',  'Ei[f{88+/dfNW*)DO!W4_LSx%_`n|+[)Td:N^t7-`iIK}SATsj`tbw=QB1-FR)Wb');
define('LOGGED_IN_KEY',    'm`cjWppy#(ozI[{sKx6A>lR,bPoq8S`?>fK/lE[E0;KOyX^Y-.?fBVdG5|cOh6E=');
define('NONCE_KEY',        '(}Xs)?r3P;4N!d+aeN%G}XoyXe@sAH-q]&rK4zQHQY{f~0zkxxEt.%f;N3&q0pGt');
define('AUTH_SALT',        ')-=t>D`$TVvq|m[5Ud-EZ02w3Y=aokGQG&]?vA{Mf-q^C?pl(RZ#<Vg4i:YQ)Fux');
define('SECURE_AUTH_SALT', '9Y=6S9|k#li^N/JMHLMkp=SD&{-44_>`-c<-wM-;/Ew]RsQX}Y<ctZ6Ep2H@v=)+');
define('LOGGED_IN_SALT',   'G+O(~zbzJot^76]{9n~j$md~5u}Y.54+:NeOd3&1~EqbXWfZ]t8Y&5!-pKr8q@ii');
define('NONCE_SALT',       'o0!6#D]}1bPePD32 |{4*ZnSW/q2g6.Gs|RQ_f{-+`pI68r(YG|bx},.m3+}Q#rR');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
