<?php

remove_filter ('the_content', 'wpautop');

function pagination() {
    global $wp_query;
    $total = $wp_query->max_num_pages; 
    $current = max(1, get_query_var('paged'));
    $pages = paginate_links( array(
		'base'      => get_pagenum_link(1) . '%_%',
        'format'    => 'page/%#%',
        'current'   => $current,
        'total'     => $total,
        'type'      => 'array',
        'prev_text' => ('«'),
        'next_text' => ('»'),
        'mid_size'  => $total
       ) );
    if( is_array( $pages ) ) {
        echo '<div class="tsp-pagination"><ul>';
        foreach ( $pages as $page ) {
            echo "<li>".$page."</li>";
        }
       	echo '</ul></div>';
    }
}

function custom_field_excerpt($word_count) {
    global $post;
    $text = get_field('content');
    if ($text != '') {
        $text = strip_shortcodes($text);
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]&gt;', ']]&gt;', $text);
        $excerpt_length = $word_count;
        $excerpt_more = apply_filters('excerpt_more', ' ');
        $text = wp_trim_words($text, $excerpt_length, $excerpt_more);
    }

    if (str_word_count($text) < $word_count){
        return apply_filters('the_excerpt', $text);
    }

    $text .= "....";
    return $text;
}

function get_category_count($args) {
    $category = get_category('Travel');
    $count = $category->category_count;
    echo $count;
}

function get_festival_year() {
    $query = new WP_Query( array( 'post_type' => 'festival_post' ) );
    $posts = $query->posts;
    $yearArray = array();
    foreach($posts as $post) {
        $date = strtotime($post->date);
        $year = date('Y', $date);
        if (!in_array($year, $yearArray)) {
            $yearArray[] = $year;
        }            
    }
    return $yearArray;
}




?>