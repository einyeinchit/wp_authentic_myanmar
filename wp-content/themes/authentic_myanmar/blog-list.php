<?php 
	/*
		Template Name: blog_list
	*/
get_header(); ?>
        
<main>
	<section class="tsp-title-breadcrumb">
		<div class="container">
			<div class="row">
				<div class="tsp-title col-md-6 col-sm-6 col-xs-12 tsp-no-padding-left">
					<?php 
						$pagename = get_query_var('pagename');
						if($pagename == 'travel'){
							echo "<h1>Travel <span>Blog</span></h1>";
						}else if($pagename == 'festivals_of_myanmar'){
							echo "<h1>Festivals <span>of Myanmar</span></h1>";
						}else if($pagename == 'social'){
							echo "<h1>Social <span>Activities</span></h1>";
						}else if($pagename == 'charity'){
							echo "<h1>Charity <span>of Authentic</span></h1>";
						}
					?>
				</div>
				<!-- div title head page -->
				<div class="tsp-breadcumb col-md-6 col-sm-6 col-xs-12 tsp-no-padding-right">
					<ul>
						<li><a href="<?php echo home_url('/'); ?>">Home</a></li>
						<li>/</li>
						<li><span>
							<?php 
								$pagename = get_query_var('pagename');
								if($pagename == 'travel'){
									echo "travel";
								}else if($pagename == 'festivals_of_myanmar'){
									echo "festivals";
								}else if($pagename == 'social'){
									echo "social";
								}else if($pagename == 'charity'){
									echo "charity";
								}
							?>
						</span>
						</li>
					</ul>
				</div>
				<!-- div breadcrumb -->
			</div>
			<!-- div row -->
		</div>
	</section>
    <section class="tsp-page-tpl">
        <div class="container blog-sidebar">
            <div class="row">
                <div class="col-sm-9 tsp-no-padding-left">
                    <div class="content-blog-sidebar">
                    
                        <?php
                            $pagename = get_query_var('pagename');
                            $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                            query_posts(array(
                                'post_type' => 'blog_post',
                                'posts_per_page' => 6,
                                'paged' => $paged,
                                'category_name' => $pagename
                            ) );
                        ?>
                        <!-- Start article of blog -->
                        <?php if (have_posts()) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <div class="blog-item">
                                    <?php $images = acf_photo_gallery('photo', $post->ID); ?>
                                    <div class="entry-image">
                                        <div class="owl-carousel owl-theme tsp-owl-carousel" data-item-lg="1" data-item-md="1" data-item-sm="1">
                                            <?php foreach( $images as $image ): ?>
                                                <div class="item">
                                                    <img src="<?php echo $image['full_image_url']; ?>" alt="<?php echo $image['title']; ?>" />
                                                </div>
                                                <!--end item-->
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <h3 class="entry-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <div class="post-meta-with-sidebar">
                                        <div class="author">by <?php the_author(); ?></div>
                                        <div class="time" style="border: 0">Post at <?php the_time('F jS, Y'); ?></div>
                                    </div>
                                    <div class="post-excerpt">
                                        <?php echo custom_field_excerpt(80); ?>
                                    </div>
                                    <a class='read-more' href="<?php echo get_permalink(); ?>">Read More</a>
                                </div>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <h1>NO CONTENT FOUND!</h1>
                        <?php endif; ?>
                        <!-- End article of blog -->
                        
                        <?php pagination(); ?>
                        <?php wp_reset_postdata(); ?>

                    </div>
                </div>
                <div class="col-sm-3 tsp-no-padding-right">
                    <div class="tsp-widget tsp-widget-blog">
                        
                        <!--Start list categories Blog-->
                        <aside class="widget tsp-widget-blog-cat">
                            <h2 class="widget-title">Category</h2>
                            <ul>
                                <?php 
                                    $categories = get_categories();
                                    foreach( $categories as $category ){
                                        if($category->name == 'Charity'){
                                            $link = home_url('/')."blog/charity";
                                        }else if($category->name == 'Social'){
                                            $link = home_url('/')."blog/social";
                                        }else{
                                            $link = home_url('/')."blog/travel";
                                        }
                                        echo '<li><a href="' . $link . '">' . $category->name . '<span>' . $category->category_count . '</span></a></li>';
                                    }
                                ?>
                            </ul>
                        </aside>
                        <!--End list categories Blog-->

                        <!--Start list archives Blog-->
                        <!-- <aside class="widget tsp-widget-blog-cat">
                            <h2 class="widget-title">Archives</h2> -->
                            <?php 
                                // $args = array(
                                //     'type'            => 'monthly',
                                //     'limit'           => '',
                                //     'format'          => 'html', 
                                //     'before'          => '',
                                //     'after'           => '',
                                //     'show_post_count' => false,
                                //     'echo'            => 1,
                                //     'order'           => 'DESC',
                                //     'post_type'     => 'blog_post');
                                // wp_get_archives( $args );
                            ?>
                        <!-- </aside> -->
                        <!--End list archives Blog-->

                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>