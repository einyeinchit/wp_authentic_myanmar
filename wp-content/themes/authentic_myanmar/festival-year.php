<?php 
	/*
		Template Name: festival_year
	*/
get_header(); ?>
        
<main>
    <section class="tsp-title-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="tsp-title col-md-6 col-sm-6 col-xs-12 tsp-no-padding-left">
                    <h1>Festivals <span>Of Myanmar</span></h1>
                </div>
                <!-- div title head page -->
                <div class="tsp-breadcumb col-md-6 col-sm-6 col-xs-12 tsp-no-padding-right">
                    <ul>
                        <li><a href="<?php echo home_url('/'); ?>">Home</a>
                        </li>
                        <li>/</li>
                        <li><span>Festivals Of Myanmar</span>
                        </li>
                    </ul>
                </div>
                <!-- div breadcrumb -->
            </div>
            <!-- div row -->
        </div>
    </section>
            
    <section id="tsp_our_services">
        <div class="container tsp-no-padding">
            <div class="row">
                <!-- Start article of festival_year -->
                <?php $years = get_festival_year(); foreach($years as $year) { ?>
                    <?php $link = add_query_arg( 'years', $year , home_url('/').'blog/festivals-of-myanmar/' ); ?>
                    <div class="col-md-4">
                        <div class="our-service-item">
                            <a href="<?php echo $link ?>">
                                <?php echo $year; ?>
                            </a>
                        </div>
                    </div>
                <?php } ?>
                <!-- End article of festival_year -->
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>