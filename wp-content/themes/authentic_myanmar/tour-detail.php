<?php
	/*
		Template Name: tour_detail
	*/
get_header(); ?>

<main id="tour_detail">
    <section class="tsp-title-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="tsp-title col-md-6 col-sm-6 col-xs-12 tsp-no-padding-left">
                    <h1>Tour <span>Detail</span></h1>
                </div>
                <!-- div title head page -->
                <div class="tsp-breadcumb col-md-6 col-sm-6 col-xs-12 tsp-no-padding-right">
                    <ul>
                        <li><a href="#">Home</a>
                        </li>
                        <li>/</li>
                        <li><span>Tour Detail</span>
                        </li>
                    </ul>
                </div>
                <!-- div breadcrumb -->
            </div>
            <!-- div row -->
        </div>
    </section>
    <section class="tsp-page-tpl">
        <div class="container content-blog-sidebar">

        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <div class="blog-item">
                    <?php $images = acf_photo_gallery('photo', $post->ID); ?>
                    <div class="entry-image">
                        <div class="owl-carousel owl-theme tsp-owl-carousel" data-item-lg="1" data-item-md="1" data-item-sm="1">
                            <?php foreach( $images as $image ): ?>
                                <div class="item">
                                    <img src="<?php echo $image['full_image_url']; ?>" alt="<?php echo $image['title']; ?>" />
                                </div>
                                <!--end item-->
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <h3><?php the_title(); ?></h3>
                    <h5><?php the_field('route'); ?></h5>
                    <div style="margin: 20px 0 0 0">
                        <ul class="nav">
                            <li class="active"><a data-toggle="tab" href="#description"><i class="fa fa-hospital-o"></i> Description</a>
                            </li>
                            <li><a data-toggle="tab" href="#planning"><i class="fa fa-paper-plane"></i> Tour Planning</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="description" class="tab-pane fade in active">
                                <div class="tsp-hotel-detail">
                                    <div class="tsp-detail-item">Price: <span><?php the_field('price'); ?></span></div>
                                    <div class="tsp-detail-item">Tour Duration:
                                        <span><?php the_field('night'); ?> Night / <?php the_field('days'); ?> Days</span>
                                    </div>
                                    <div class="tsp-detail-item">Transport: <span>by <?php the_field('transport'); ?></span></div>
                                    <div class="tsp-detail-item">Hotel Type:</span>
                                        <div class="tsp-star-rating">
                                            <?php
                                                $rating = (int)get_field('hotel_type');
                                                $content = "<span class='tsp-hotel-rating'>";
                                                for($a=0; $a<$rating; $a++){
                                                    $content .= "<i class='fa fa-star'></i>";
                                                }
                                                $content .= "</span>";
                                                for($b=5; $b>$rating; $b--){
                                                    $content .= "<i class='fa fa-star'></i>";
                                                }
                                                echo $content;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php the_field('content'); ?>
                            </div>
                            <!-- End Description -->
                            <div id="planning" class="tab-pane fade">
                                <?php the_field('tour_planning'); ?>
                            </div>
                            <!-- End Tour Planning -->
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>           
		<?php else : ?>
            <h1>NO CONTENT FOUND!</h1>
		<?php endif; ?>	
            
        </div>
    </section>
</main>

<?php get_footer(); ?>