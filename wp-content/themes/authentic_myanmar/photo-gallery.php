<?php 
	/*
		Template Name: photo_gallery
	*/
get_header(); ?>

<main>
    <section class="tsp-title-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="tsp-title col-md-6 col-sm-6 col-xs-12 tsp-no-padding-left">
                    <h1>Photo <span>Gallery</span></h1>
                </div>
                <!-- div title head page -->
                <div class="tsp-breadcumb col-md-6 col-sm-6 col-xs-12 tsp-no-padding-right">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li>/</li>
                        <li><span>Photo Gallery</span></li>
                    </ul>
                </div>
                <!-- div breadcrumb -->
            </div>
            <!-- div row -->
        </div>
    </section>
    <section class="tsp-page-tpl">
        <div class="container">
            <div class="row">
                
                <?php
                    $pagename = get_query_var('pagename');
                    $meta_value = (get_query_var('pagename') != 'all') ? get_query_var('pagename') : null;
                    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                    query_posts(array(
                        'post_type' => 'photo_gallery_post',
                        'posts_per_page' => 6,
                        'paged' => $paged,
                        'meta_key' => 'category',
                        'meta_value' => $meta_value
                    ) );
                ?>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-gallery">
                    <li class="<?php echo $pagename == 'all' ? 'active' : null ?>"><a href="<?php echo home_url('/'); ?>blog/photo-gallery/all">All</a></li>
                    <li class="<?php echo $pagename == 'travel' ? 'active' : null ?>"><a href="<?php echo home_url('/'); ?>blog/photo-gallery/travel">Travel</a></li>
                    <li class="<?php echo $pagename == 'social' ? 'active' : null ?>"><a href="<?php echo home_url('/'); ?>blog/photo-gallery/social">Social</a></li>
                    <li class="<?php echo $pagename == 'charity' ? 'active' : null ?>"><a href="<?php echo home_url('/'); ?>blog/photo-gallery/charity">Charity</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="gallery-content-page">
                    <div class="row">

                        <!-- Start article of photo gallery -->
                        <?php if (have_posts()) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <div class="col-md-4 col-sm-6 col-xs-6">   
                                    <div class="gallery-item">
                                        <img src="<?php the_field('photo'); ?>">
                                        <div class="gallery-action">
                                            <a href="<?php the_field('photo'); ?>" data-fancybox="images" data-caption="<?php the_title(); ?>"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>    
                                </div>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <h1>NO CONTENT FOUND!</h1>
                        <?php endif; ?>
                        <!-- End article of photo gallery -->

                    </div>
    
                    <?php pagination(); ?>
                    <?php wp_reset_postdata(); ?>
                </div>

            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>