<footer>
		<!--start footer main-->
		<div class="tsp-footer-main">
			<div class="container">
				<div class="row">
					<!--Start Item footer main-->
					<div class="col-xs-12 col-sm-6 col-md-4 tsp-footer-option tsp-no-padding-left">
						<aside class="tsp-widget-footer">
							<h2>Authentic About</h2>
							<div class="tsp-content-item">
								<p>You love to travel, and we love to help make travel one of life’s greatest pleasures. That’s why millions of consumers search for and book a wide range of hotels, flights, car rentals, cruises, vacation packages and destination activities with us.</p>
								<ul class="tsp-social-footer hidden-xs">
									<li>
										<a href="https://www.facebook.com/authenticmyanmar" title="Facebook" target="_blank"><i class="fa fa-facebook-square"></i></a>
									</li>
									<li>
										<a href="https://twitter.com/sales_authentic" title="Twitter" target="_blank"><i class="fa fa-twitter-square"></i></a>
									</li>
									<li>
										<a href="https://www.linkedin.com/authwall?trk=ripf&trkInfo=AQEHGEKnjvZApwAAAWivqcdwlKB_RXilR3pgrB0QcmAVknzPSU5D00aFJaedlLUPrAHsr_HWPjS5CtB3gVULw2SJdQZ7cxvg4LwO7Z0e73vSaYUmR-TFVkkThdWBiyvbwcTFcWE=&originalReferer=http://authentiquemyanmar.com/&sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Fauthentic-myanmar-travel-%26-tours%3Ftrk%3Dprof-following-company-logo" title="LinkedIn" target="_blank"><i class="fa fa-linkedin-square"></i></a>
									</li>
									<li>
										<a href="https://plus.google.com/u/0/+AuthenticMyanmar" title="Google plus" target="_blank"><i class="fa fa-google-plus-square"></i></a>
									</li>
									<li>
										<a href="https://www.youtube.com/channel/UCsKnFVzTKkLzC3iddc7yRSw/feed?view_as=public" title="Youtube" target="_blank"><i class="fa fa-youtube-square"></i></a>
									</li>
								</ul>
							</div>
						</aside>
					</div>
					<!--End item main footer-->
					<!--Start Item footer main-->
					<div class="col-xs-12 col-sm-6 col-md-3 tsp-footer-option">
							<aside class="tsp-widget-footer">
								<h2>Contact Us</h2>
								<div class="tsp-content-item">
									<ul class="tsp-footer-address">
										<li>
											<i class="fa fa-map-marker"><span>Address:</span></i>
											<address>
												46/B, Mahar Zaya St,
												Kyee Taw Housing,
												Kyee Taw Ward,
												Mingalar Taungnyunt Township,
												Yangon, Myanmar.
											</address>
										</li>
										<li>
											<a href="#" target="_blank">
												<i class="fa fa-phone"><span>Phone:</span></i> +95 1 20 25 64
											</a>
										</li>
										<li>
											<a href="mailto:themespond@gmail.com" target="_blank">
												<i class="fa fa-envelope"><span>Email:</span></i> contact@authentiquemyanmar.com
											</a>
										</li>
									</ul>
								</div>
							</aside>
					</div>
					<!--End item main footer-->

					<!--Start Item footer main-->
					<div class="col-xs-12 col-sm-6 col-md-3 tsp-footer-option hidden-xs hidden-sm tsp-no-padding-right">
						<aside class="tsp-widget-footer">
							<h2>Recognized by</h2>
							<div class="tsp-content-item">
								<ul class="tsp-image-instagram">
									<li>
										<a href="https://www.facebook.com/thatoe.libraries.foundation/" target="_blank"><img src="<?php bloginfo('template_directory'); ?> /img/footer/logo1.jpg" alt="supported by the International Trade Centre’s (ITC) NTF III Myanmar inclusive tourism programme"></a>
									</li>
									<li>
										<a href="http://authentiquemyanmar.com/team-authentic/#" target="_blank"><img src="<?php bloginfo('template_directory'); ?> /img/footer/logo2.jpg" alt="ASTA"></a>
									</li>
									<li>
										<a href="http://authentiquemyanmar.com/team-authentic/#" target="_blank"><img src="<?php bloginfo('template_directory'); ?> /img/footer/logo3.jpg" alt="UN Global Compact"></a>
									</li>
									<li>
										<a href="http://www.thecode.org/" target="_blank"><img src="<?php bloginfo('template_directory'); ?> /img/footer/logo4.jpg" alt="The Code"></a>
									</li>
									<li>
										<a href="https://docs.google.com/spreadsheet/ccc?key=0AlLXoeJVbfXedFNGQlgwMVNjMnpoRkp4QUJLRERLdkE&usp=sharing#gid=9" target="_blank"><img src="<?php bloginfo('template_directory'); ?> /img/footer/logo5.jpg" alt="UMTA"></a>
									</li>
								</ul>
							</div>
						</aside>
					</div>
					<!--End item main footer-->
				</div>
				<!--row-->
			</div>
			<!--container-->
		</div>
		<!--end footer main-->
		
		<!--start footer bar-->
		<div class="tsp-footer-bar">
				<div class="container">
					<div class="row">
						<!--start box text copyright-->
						<div class="tsp-copyright col-md-6 col-sm-6 col-xs-12 tsp-no-padding-left">
							<p>Copyright <span>&copy;</span> 2018 PanRays. All Rights Reserved</p>
						</div>
						<!--End box text copyright-->
						<!--Start box give news-->
						<div class="tsp-menu-footer col-md-6 col-sm-6 col-xs-12 tsp-no-padding-right">
							<ul>
								<li><a href="<?php echo home_url('/'); ?>">Home</a>
								</li>
								<li><a href="<?php echo home_url('/'); ?>hotel">Hotel</a>
								</li>
								<li><a href="<?php echo home_url('/'); ?>tour/reservation">Reservation</a>
								</li>
								<li><a href="<?php echo home_url('/'); ?>blog/travel">Travel Blog</a>
								</li>
								<li><a href="<?php echo home_url('/'); ?>contact">Contact</a>
								</li>
							</ul>
						</div>
						<!--End box give news-->
					</div>
					<!--row-->
				</div>
				<!--container-->
		</div>
		<!--End footer bar-->
</footer>

</div>

<script src="<?php bloginfo('template_url'); ?>/js/vendors/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/vendors/modernizr.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/vendors/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/vendors/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/vendors/jquery.slicknav.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/vendors/ckeditor/ckeditor.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/vendors/fancybox.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/common.js"></script>

<?php wp_footer(); ?>
</body>
</html>