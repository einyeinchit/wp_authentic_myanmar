<?php
	/*
		Template Name: blog_detail
	*/
get_header(); ?>

<main>
    <section class="tsp-title-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="tsp-title col-md-6 col-sm-6 col-xs-12 tsp-no-padding-left">
                    <h1>Blog <span>Detail</span></h1>
                </div>
                <!-- div title head page -->
                <div class="tsp-breadcumb col-md-6 col-sm-6 col-xs-12 tsp-no-padding-right">
                    <ul>
                        <li><a href="#">Home</a>
                        </li>
                        <li>/</li>
                        <li><span>Blog Detail</span>
                        </li>
                    </ul>
                </div>
                <!-- div breadcrumb -->
            </div>
            <!-- div row -->
        </div>
    </section>
    <section class="tsp-page-tpl">
        <div class="container content-blog-sidebar">
            <div class="row">
                <div class="col-sm-9 tsp-no-padding-left">
                    <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                            <div class="blog-item">
                                <?php $images = acf_photo_gallery('photo', $post->ID); ?>
                                <div class="entry-image">
                                    <div class="owl-carousel owl-theme tsp-owl-carousel" data-item-lg="1" data-item-md="1" data-item-sm="1">
                                        <?php foreach( $images as $image ): ?>
                                            <div class="item">
                                                <img src="<?php echo $image['full_image_url']; ?>" alt="<?php echo $image['title']; ?>" />
                                            </div>
                                            <!--end item-->
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <h3><?php the_title(); ?></h3>
                                <div class="post-meta-with-sidebar">
                                    <div class="author">by <?php the_author(); ?></div>
                                    <div class="time" style="border: 0">Post at <?php the_time('F jS, Y'); ?></div>
                                </div>
                                <p><?php the_field("content"); ?></p>
                            </div>
                        <?php endwhile; ?>
                        <div class="pre_link clearfix">
                            <div class="prev"><i class="fa fa-angle-left"></i> <?php previous_post_link( '%link','Previous Post' ) ?></div>
                            <div class="next"><?php next_post_link( '%link','Next Post' ) ?> <i class="fa fa-angle-right"></i></div>
                        </div>            
                    <?php else : ?>
                            <h1>NO CONTENT FOUND!</h1>
                    <?php endif; ?>	
                </div>
                <div class="col-sm-3 tsp-no-padding-right">
                    <div class="tsp-widget tsp-widget-blog">
                        <!--Start list categories Blog-->
                        <aside class="widget tsp-widget-blog-cat">
                            <h2 class="widget-title">Category</h2>
                            <ul>
                                <?php 
                                    $categories = get_categories();
                                    foreach( $categories as $category ){
                                        if($category->name == 'Charity'){
                                            $link = home_url('/')."blog/charity";
                                        }else if($category->name == 'Social'){
                                            $link = home_url('/')."blog/social";
                                        }else{
                                            $link = home_url('/')."blog/travel";
                                        }
                                        echo '<li><a href="' . $link . '">' . $category->name . '<span>' . $category->category_count . '</span></a></li>';
                                    }
                                ?>
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </div>        
    </section>
</main>

<?php get_footer(); ?>