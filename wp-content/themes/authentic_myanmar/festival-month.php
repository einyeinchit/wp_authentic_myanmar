<?php 
	/*
		Template Name: festival_month
	*/
get_header(); ?>
        
<main>
    <section class="tsp-title-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="tsp-title col-md-6 col-sm-6 col-xs-12 tsp-no-padding-left">
                    <h1>Festivals <span>Of Myanmar</span></h1>
                </div>
                <!-- div title head page -->
                <div class="tsp-breadcumb col-md-6 col-sm-6 col-xs-12 tsp-no-padding-right">
                    <ul>
                        <li><a href="<?php echo home_url('/'); ?>">Home</a>
                        </li>
                        <li>/</li>
                        <li><span>Festivals Of Myanmar</span>
                        </li>
                    </ul>
                </div>
                <!-- div breadcrumb -->
            </div>
            <!-- div row -->
        </div>
    </section>

    <section id="tsp_our_services">
        <div class="container tsp-no-padding">
            <div class="row">
                 <!-- Start article of festival_month -->
                <?php 
                    for ($i=1; $i<=12; $i++) {
                        switch ($i) {
                            case 1:
                                $monthName = 'Jan';
                                break;
                            case 2:
                                $monthName = 'Feb';
                                break;
                            case 3:
                                $monthName = 'Mar';
                                break;
                            case 4:
                                $monthName = 'Apr';
                                break;
                            case 5:
                                $monthName = 'May';
                                break;
                            case 6:
                                $monthName = 'Jun';
                                break;
                            case 7:
                                $monthName = 'Jul';
                                break;
                            case 8:
                                $monthName = 'Aug';
                                break;
                            case 9:
                                $monthName = 'Sep';
                                break;
                            case 10:
                                $monthName = 'Oct';
                                break;
                            case 11:
                                $monthName = 'Nov';
                                break;
                            case 12:
                                $monthName = 'Dec';
                                break;             
                        }
                        $years = $_REQUEST['years'];
                        $array = array(
                            'years' => $years,
                            'months' => $i,
                        );
                        $link = add_query_arg( $array , home_url('/').'blog/festivals-of-myanmar/' );
                ?>
                    <div class="col-md-4">
                        <div class="our-service-item">
                            <a href="<?php echo $link ?>">
                                <?php echo $monthName ?>
                            </a>
                        </div>
                    </div>
                <?php } ?>
                <!-- End article of festival_month -->
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>