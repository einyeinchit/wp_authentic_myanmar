<?php 
	/*
		Template Name: festivals_list
    */

    $years = $_REQUEST['years'];
    $months = $_REQUEST['months'];

    // echo 'years ==>'.$years.'<br>';
    // echo 'months ==>'.$months;

get_header(); ?>

<main>
    <section class="tsp-title-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="tsp-title col-md-6 col-sm-6 col-xs-12 tsp-no-padding-left">
                    <h1><?php echo $months ?> / <span><?php echo $years ?></span></h1>
                </div>
                <!-- div title head page -->
                <div class="tsp-breadcumb col-md-6 col-sm-6 col-xs-12 tsp-no-padding-right">
                    <ul>
                        <li><a href="<?php echo home_url('/'); ?>">Home</a>
                        </li>
                        <li>/</li>
                        <li><span>Festivals Of Myanmar</span>
                        </li>
                    </ul>
                </div>
                <!-- div breadcrumb -->
            </div>
            <!-- div row -->
        </div>
    </section>
    <div class="tsp-page-tpl">
        <section id="tsp_hotel_grid" class="tsp-hotel-list tsp-grid-hotel">
            <div class="container">
                <div class="row">
                
                    <div class="tsp-box-hotel-list">
                        <?php
                            $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                            // FirstDay
                            $firstDay = date('Y-m-d', strtotime($months.'/'.'01/'.$years));
                            // LastDay
                            $date = new DateTime($months.'/'.'01/'.$years);
                            $date->modify('last day of this month');
                            $lastDay = $date->format('Y-m-d');
                            query_posts(array(
                                'post_type'      => 'festival_post',
                                'posts_per_page' => 6,
                                'paged'          => $paged,
                                // 'meta_key' => 'category',
                                // 'meta_value' => 'Event',
                                'meta_query' => array(
                                    'relation'    => 'AND',
                                    array(
                                      'key'       => 'date',
                                      'value' => array($firstDay, $lastDay),
                                      'compare' => 'BETWEEN', 
                                      'type'      => 'DATE'
                                    )
                                )
                            ) );
                        ?>
                        <!-- Start article of hotel -->
                        <?php if (have_posts()) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <article class="col-md-4 col-sm-4 col-xs-6 tsp-full-xs tsp-hotel-item">
                                    <div class="tsp-image">
                                        <div class="tsp-img">
                                            <img src="<?php the_field('photo'); ?>" alt="<?php the_title(); ?>">
                                        </div>
                                    </div>
                                    <div class="tsp-hotel-content">
                                        <h2><a href="#"><?php the_title(); ?></a></h2>
                                        <div class="tsp-price-rate">
                                            <div class="tsp-price col-md-6 col-sm-6 col-xs-6">
                                                <strong><?php the_field('date'); ?>&nbsp;&nbsp;</strong>
                                            </div>
                                        </div>
                                        <p><?php the_field('content'); ?>&nbsp;&nbsp;</p>
                                        <a class='read-more' href="<?php echo get_permalink(); ?>">Read More</a>
                                    </div>
                                </article>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <h1>NO CONTENT FOUND!</h1>
                        <?php endif; ?>
                        <!-- End article of hotel -->
                    </div>

                    <?php pagination(); ?>
                    <?php wp_reset_postdata(); ?>
                    
                </div>
            </div>
        </section>
    </div>
</main>

<?php get_footer(); ?>