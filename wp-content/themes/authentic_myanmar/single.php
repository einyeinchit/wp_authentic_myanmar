<?php
	/*
		Template Name: single
	*/

	$post_type = get_post_type();
	if ( $post_type == 'blog_post' ) {
   		include(TEMPLATEPATH . '/blog-detail.php');
	} else if ( $post_type == 'tour_post' ) {
		include(TEMPLATEPATH . '/tour-detail.php');
	}

get_header(); ?>

<?php get_footer(); ?>
