<?php 
	/*
		Template Name: festivals_of_myanmar
    */

    $years = $_REQUEST['years'];
    $months = $_REQUEST['months'];
    if($years == 0){
        include(get_template_directory().'/festival-year.php'); 
    }else if($years != 0 && $months == 0){
        include(get_template_directory().'/festival-month.php');
    }else if($months != 0){
        include(get_template_directory().'/festival-list.php');
    }

get_header(); ?>

<?php get_footer(); ?>