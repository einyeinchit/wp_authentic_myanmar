﻿<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes, minimum-scale=1.0, maximum-scale=3.0">
<meta name="robots" content="noindex">
<meta name="description" content="">
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/img/favicon.png"/>
<link href="https://fonts.googleapis.com/css?family=Patua+One%7COpen+Sans:300,300i,400,400i,600,600i,700,700i,800,800i%7CRoboto+Slab:100,300,400,700Lato:100,100i,300,300i,400,400i,700,700i,900,900i&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/vendors/bootstrap.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/vendors/font-awesome.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/vendors/owl.carousel.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/vendors/owl.theme.green.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/vendors/animate.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/vendors/slicknav.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/vendors/fancybox.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/common/main.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/customize.css">

<?php wp_head(); ?>
</head>
<body>
  <!--[if lt IE 10]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
 <div class="tsp-main">
        <header>
            <!--start header main-->
            <div class="tsp-header-main">
                <div class="container">
                    <div class="row">
                        <div class="tsp-logo col-sm-3 col-xs-4">
                            <a href="<?php bloginfo('url'); ?>">
                                <img src="<?php bloginfo('template_directory'); ?> /img/logo.png" alt="Logo">
                            </a>
                        </div>
                        <div id="tsp_menu_cart_search" class="col-md-9 col-sm-9 col-xs-12">
                            <nav>
                                <ul id="menu">
                                    <?php $page = get_the_title($post->post_parent); ?>
                                    <li class="<?php echo is_home() ? 'current-menu-item' : null ?>">
                                        <a href="<?php echo home_url('/'); ?>">Home</a>
                                    </li>
                                    <li class="<?php echo ($page == 'about') ? 'current-menu-item' : null ?>">
                                        <a href="<?php echo home_url('/'); ?>about/team">About Us</a>
                                        <!-- <ul>
                                            <li><a href="<?php echo home_url('/'); ?>about/general-information">General Information</a></li>
                                            <li><a href="<?php echo home_url('/'); ?>about/team">Team Authentic</a></li>
                                        </ul> -->
                                    </li>
                                    <li class="<?php echo ($page == 'hotel') ? 'current-menu-item' : null ?>">
                                        <a href="<?php echo home_url('/'); ?>hotel">Hotel</a>
                                    </li>
                                    <li class="<?php echo ($page == 'tour') ? 'current-menu-item' : null ?>">
                                        <a>Tour</a>
                                        <ul>
                                            <li><a href="<?php echo home_url('/'); ?>tour/reservation"> Reservation</a></li>
                                            <li><a href="<?php echo home_url('/'); ?>tour/highlight"> Highlight Tour</a></li>
                                            <li><a href="<?php echo home_url('/'); ?>tour/programs"> Tour Programs</a></li>
                                        </ul>
                                    </li>
                                    <li class="<?php echo ($page == 'blog' || $page == 'photo-gallery') ? 'current-menu-item' : null ?>">
                                        <a>Blog</a>
                                        <ul>
                                            <li><a href="<?php echo home_url('/'); ?>blog/travel">Travel</a></li>
                                            <li><a href="<?php echo home_url('/'); ?>blog/festivals-of-myanmar/"> Festivals of Myanmar</a></li>
                                            <li class="tsp-has-menu-child">
                                                <a>Activity</a>
                                                <ul>
                                                    <li><a href="<?php echo home_url('/'); ?>blog/social"> Social</a></li>
                                                    <li><a href="<?php echo home_url('/'); ?>blog/charity"> Charity</a></li>
                                                    <li><a href="<?php echo home_url('/'); ?>blog/photo-gallery/all"> Photo Gallery</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                        
                                    </li>
                                    <li class="<?php echo ($page == 'contact') ? 'current-menu-item' : null ?>"><a href="<?php echo home_url('/'); ?>contact">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!--div nav cart search-->
                    </div>
                    <!--row-->
                </div>
                <!--container-->
            </div>
            <!--End header main-->
        </header>
        <!--header-->