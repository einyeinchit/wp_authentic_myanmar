<?php
	/*
		Template Name: tour_list
	*/
get_header(); ?>

<main>
    <section class="tsp-title-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="tsp-title col-md-6 col-sm-6 col-xs-12 tsp-no-padding-left">
                    <?php 
                        $pagename = get_query_var('pagename');
                        echo ($pagename == 'highlight') ? '<h1>Highlight <span>Tour</span></h1>' : '<h1>Tour <span>Programs</span></h1>'
                    ?>
                </div>
                <!-- div title head page -->
                <div class="tsp-breadcumb col-md-6 col-sm-6 col-xs-12 tsp-no-padding-right">
                    <ul>
                        <li><a href="<?php echo home_url('/'); ?>">Home</a></li>
                        <li>/</li>
                        <li><span>
                            <?php 
                                $pagename = get_query_var('pagename');
                                echo ($pagename == 'highlight') ? 'Highlight Tour' : 'Tour Programs'
                            ?>
                        </span>
                        </li>
                    </ul>
                </div>
                <!-- div breadcrumb -->
            </div>
            <!-- div row -->
        </div>
    </section>
    <div class="tsp-page-tpl">
        <section id="tsp_hotel_grid" class="tsp-hotel-list tsp-grid-hotel">
            <div class="container">                     
                <div class="row">
                
                    <?php
                        $pagename = get_query_var('pagename');
                        $meta_value = ($pagename == 'highlight') ? 'Highlight' : null;
                        $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                        query_posts(array(
                            'post_type' => 'tour_post',
                            'posts_per_page' => 6,
                            'paged' => $paged,
                            'meta_key' => 'category',
                            'meta_value' => $meta_value
                        ) );
                    ?>
                    <!-- Start article of tour -->
                    <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                            <article class="col-md-4 col-sm-4 col-xs-6 tsp-full-xs tsp-hotel-item">
                                <div class="tsp-image">
                                    <div class="tsp-img">
                                        <img src="<?php the_field('cover'); ?>" alt="<?php the_title(); ?>">
                                    </div>
                                    <div class="tour_price">
                                        <?php the_field('price'); ?> / <?php the_field('days'); ?> days
                                    </div>
                                </div>
                                <div class="tsp-hotel-content">
                                    <h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
                                    <p style="text-transform: none; font-size: 14px; color: #999999; font-weight: normal; letter-spacing: 0.3px;"><?php the_field('route'); ?></p>
                                    <p><?php echo custom_field_excerpt(50); ?></p>
                                </div>
                                <a class='read-more' href="<?php echo get_permalink(); ?>">Read More</a>
                            </article>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <h1>NO CONTENT FOUND!</h1>
                    <?php endif; ?>
                    <!-- End article of tour -->
                         
                    <?php pagination(); ?>
                    <?php wp_reset_postdata(); ?>

                </div>
            </div>  
        </section>
    </div>
</main>

<?php get_footer(); ?>