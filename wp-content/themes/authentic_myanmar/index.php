<?php
	/*
		Template Name: home
	*/
get_header(); ?>
<!-- <audio src="<?php bloginfo('template_url'); ?>/piano-melody.mp3 ?>" autoplay>
	<p>If you are reading this, it is because your browser does not support the audio element.</p>
</audio> -->
<main>
	<section id="tsp_slider">
		<div id="tsp_slider_main">
			<?php echo do_shortcode("[metaslider id=432]"); ?>
		</div>
	</section>
	<section id="tsp_info_box">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="info-box-item">
						<div class="info-box-icon"><i class="fa fa-paper-plane"></i>
						</div>
						<h3>Our Vision</h3>
						<p>Sustainable tourism can protect the country’s natural and cultural riches and create economic opportunity.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="info-box-item">
						<div class="info-box-icon"><i class="fa fa-map-o"></i>
						</div>
						<h3>Our Mission</h3>
						<p>Avoid Negative impact; improve human resources commitment of authenticity through travel and tourism.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="info-box-item">
						<div class="info-box-icon"><i class="fa fa-globe"></i>
						</div>
						<h3>Our Strategy</h3>
						<p>Sustainable Tourism is our objective and will create a remarkable responsible team in Myanmar tourism.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="tsp_hot_tour">
		<?php
			$paged = get_query_var('paged') ? get_query_var('paged') : 1;
			query_posts(array(
				'post_type' => 'tour_post',
				'paged' => $paged,
				'meta_key' => 'category',
				'meta_value' => 'Highlight'
			) );
		?>
		<div class="container">
			<div class="row">
				<div class="tsp-title-home">
					<h2>Highlight Tour</h2>
				</div>
				<div class="hot-tours-shortcode">
					<div class="owl-carousel owl-theme tsp-owl-carousel" data-item-lg="3" data-item-md="2" data-item-sm="1">
						<!-- Start article of tour -->
						<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>
								<div class="item hot-tours-shortcode-item">
									<img src="<?php the_field('cover'); ?>">
									<div class="info-summary">
										<span class="name-add"><?php the_title(); ?></span>
										<span class="price-add"><?php the_field('price'); ?></span>
									</div>
									<div class="info-hidden">
										<h3><?php the_title(); ?></h3>
										<div class="meta"><?php the_field('night'); ?> Night + <?php echo get_field('flight') == '1' ? 'Flight + ' : null; ?> <?php the_field('hotel_type'); ?>*Hotel</div>
										<div class="price"><?php the_field('price'); ?></div>
										<!-- <a href="#">Read More</a> -->
									</div>
								</div>
								<!--end item-->
							<?php endwhile; ?>
						<?php else : ?>
							<h1>NO CONTENT FOUND!</h1>
						<?php endif; ?>
                    	<!-- End article of tour -->
					</div>
					<div class="all-offers">
						<a href="<?php echo home_url('/'); ?>tour/highlight">Read More</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="tsp_testimonial_list" class="our-testimonials-wrapper">
		<div class="container">
			<div class="row">
				<div class="tsp-title-home">
					<h2>Our Testimonials</h2>
				</div>
				<div class="owl-carousel owl-theme tsp-owl-carousel our-testimonials" data-item-lg="3" data-item-md="2" data-item-sm="1">
					<div class="item our-testimonials-item">
						<img src="http://placehold.it/100x100" alt="testimonial 1">
						<div class="info-summary-testimonial">
							<h3><a href="#">Christine Ha</a></h3>
							<div class="testimonial-rated">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<p>
								Best service ever. I could enjoy a wide range of special foods, comfortable room here.
							</p>
						</div>
					</div>
					<!--end item-->
					<div class="item our-testimonials-item">
						<img src="http://placehold.it/100x100" alt="testimonial 2">
						<div class="info-summary-testimonial">
							<h3><a href="#">Nick Jonas</a></h3>
							<div class="testimonial-rated">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<p>
								Thank you very much for your support to help me to have an unforgettable trip with my friends.
							</p>
						</div>
					</div>
					<!--end item-->
					<div class="item our-testimonials-item">
						<img src="http://placehold.it/100x100" alt="testimonial 3">
						<div class="info-summary-testimonial">
							<h3><a href="#">Selena Gomez</a></h3>
							<div class="testimonial-rated">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<p>
								I highly recommend your thoughtful services with many big deals. Thanks a lot!
							</p>
						</div>
					</div>
					<!--end item-->
					<div class="item our-testimonials-item">
						<img src="http://placehold.it/100x100" alt="testimonial 4">
						<div class="info-summary-testimonial">
							<h3><a href="#">Nick Jonas</a></h3>
							<div class="testimonial-rated">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<p>
								Thank you very much for your support to help me to have an unforgettable trip with my friends.
							</p>
						</div>
					</div>
					<!--end item-->
				</div>
			</div>
		</div>
	</section>
	<section id="tsp_served">
		<div class="container">
			<div class="row">
				<div class="tsp-title-home">
					<h2>We Are Proud To Have Served</h2>
				</div>
				<div class="tsp-content-counter">
					<div class="tsp-countUp col-md-2 col-sm-4 col-xs-6 tsp-full-xs tsp-no-padding-left tsp-item-1">
						<div class="tsp-box-counter">
							<i class="fa fa-suitcase"></i>
							<div class="tsp-count">
								<p class="tsp-number">123</p>
								<p class="tsp-section">Tour</p>
							</div>
						</div>
					</div>
					<div class="tsp-countUp col-md-2 col-sm-4 col-xs-6 tsp-full-xs tsp-item-1">
						<div class="tsp-box-counter">
							<i class="fa fa-empire"></i>
							<div class="tsp-count">
								<p class="tsp-number">536</p>
								<p class="tsp-section">Holidays</p>
							</div>
						</div>
					</div>
					<div class="tsp-countUp col-md-2 col-sm-4 col-xs-6 tsp-full-xs tsp-item-1">
						<div class="tsp-box-counter">
							<i class="fa fa-empire"></i>
							<div class="tsp-count">
								<p class="tsp-number">439</p>
								<p class="tsp-section">Hotels</p>
							</div>
						</div>
					</div>
					<div class="tsp-countUp col-md-2 col-sm-4 col-xs-6 tsp-full-xs tsp-item-1">
						<div class="tsp-box-counter">
							<i class="fa fa-building-o"></i>
							<div class="tsp-count">
								<p class="tsp-number">296</p>
								<p class="tsp-section">Flights</p>
							</div>
						</div>
					</div>
					<div class="tsp-countUp col-md-2 col-sm-4 col-xs-6 tsp-full-xs tsp-item-1">
						<div class="tsp-box-counter">
							<i class="fa fa-birthday-cake"></i>
							<div class="tsp-count">
								<p class="tsp-number">439</p>
								<p class="tsp-section">Parties</p>
							</div>
						</div>
					</div>
					<div class="tsp-countUp col-md-2 col-sm-4 col-xs-6 tsp-full-xs tsp-no-padding-right tsp-item-1">
						<div class="tsp-box-counter">
							<i class="fa fa-smile-o"></i>
							<div class="tsp-count">
								<p class="tsp-number">2689</p>
								<p class="tsp-section">Happy customers</p>
							</div>
						</div>
					</div>
				</div>
				<!-- dic box content served -->
			</div>
			<!-- div row -->
		</div>
	</section>
	<section id="tsp_our_blog_list_home" class="shortcode-our-blog-warpper">
		<?php
			$paged = get_query_var('paged') ? get_query_var('paged') : 1;
			query_posts(array(
				'post_type' => 'blog_post',
				'posts_per_page' => 4,
				'paged' => $paged
			) );
		?>
		<div class="container">
			<div class="row">
				<div class="tsp-title-home">
					<h2>Our Blog</h2>
				</div>
				<div class="owl-carousel owl-theme tsp-owl-carousel shortcode-our-blog" data-item-lg="2" data-item-md="2" data-item-sm="1">
					
					<!-- Start article of blog -->
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
							<div class="item our-blog-shortcode-item">
								<div class="img_wrap">
									<?php $images = explode(",", get_field('photo')); ?>
									<?php echo wp_get_attachment_image( $images[0], 'full' ); ?>
								</div>	
								<div class="info-summary-blog">
									<h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php echo custom_field_excerpt(10); ?></p>
									<div class="read-more"><a href="<?php echo get_permalink(); ?>">Read More</a></div>
								</div>
							</div>
							<!--end item-->
						<?php endwhile; ?>
					<?php else : ?>
						<h1>NO CONTENT FOUND!</h1>
					<?php endif; ?>
					<!-- End article of blog -->
                        				
				</div>
			</div>
		</div>
	</section>
</main>

<?php get_footer(); ?>