<?php
	/*
		Template Name: hotel_list
	*/
get_header(); ?>

<main>
    <section class="tsp-title-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="tsp-title col-md-6 col-sm-6 col-xs-12 tsp-no-padding-left">
                    <h1>Hotel <span>List</span></h1>
                </div>
                <!-- div title head page -->
                <div class="tsp-breadcumb col-md-6 col-sm-6 col-xs-12 tsp-no-padding-right">
                    <ul>
                        <li><a href="<?php echo home_url('/'); ?>">Home</a>
                        </li>
                        <li>/</li>
                        <li><span>Hotel</span>
                        </li>
                    </ul>
                </div>
                <!-- div breadcrumb -->
            </div>
            <!-- div row -->
        </div>
    </section>
    <div class="tsp-page-tpl">
        <section id="tsp_hotel_grid" class="tsp-hotel-list tsp-grid-hotel">
            <div class="container">
                <div class="row">
                
                    <div class="tsp-box-hotel-list">
                        <?php
                            $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                            query_posts(array(
                                'post_type' => 'hotel_post',
                                'posts_per_page' => 6,
                                'paged' => $paged
                            ) );
                        ?>
                        <!-- Start article of hotel -->
                        <?php if (have_posts()) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <article class="col-md-4 col-sm-4 col-xs-6 tsp-full-xs tsp-hotel-item">
                                    <div class="tsp-image">
                                        <div class="tsp-img">
                                            <img src="<?php the_field('photo'); ?>" alt="<?php the_title(); ?>">
                                        </div>
                                    </div>
                                    <div class="tsp-hotel-content">
                                        <h2><a href="#"><?php the_title(); ?></a></h2>
                                        <div class="tsp-price-rate">
                                            <div class="tsp-price col-md-6 col-sm-6 col-xs-6">
                                                <strong><?php the_field('price'); ?>&nbsp;&nbsp;</strong><span> per night</span>
                                            </div>
                                            <div class="tsp-star-rating col-md-6 col-sm-6 col-xs-6">
                                                <?php
                                                    $rating = (int)get_field('rating');
                                                    $content = "<span class='tsp-hotel-rating'>";
                                                    for($a=0; $a<$rating; $a++){
                                                        $content .= "<i class='fa fa-star'></i>";
                                                    }
                                                    $content .= "</span>";
                                                    for($b=5; $b>$rating; $b--){
                                                        $content .= "<i class='fa fa-star'></i>";
                                                    }
                                                    echo $content;
                                                ?>
                                            </div>
                                        </div>
                                        <p><?php the_field('content'); ?>&nbsp;&nbsp;</p>
                                        <a class='read-more' href="<?php echo get_permalink(); ?>">Read More</a>
                                    </div>
                                </article>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <h1>NO CONTENT FOUND!</h1>
                        <?php endif; ?>
                        <!-- End article of hotel -->
                    </div>

                    <?php pagination(); ?>
                    <?php wp_reset_postdata(); ?>
                    
                </div>
            </div>
        </section>
    </div>
</main>

<?php get_footer(); ?>